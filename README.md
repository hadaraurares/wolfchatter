# Wolfchatter

## Run the project

1. Install nodejs latest stable or LTS version.
2. Run `npm install -g @angular/cli`
3. Run `npm install` inside the project folder.
4. Run `ng serve --open`. In case the browser will not open the page for you, you can find it at `http://localhost:4200/`.

## Short description

This project was generated using Angular CLI, latest Angular version to date (`7`). Leaflet.js library was used but under an open-source Angular module (`https://github.com/Asymmetrik/ngx-leaflet`). `localStorage` is used to store the messages.

### External dependencies

`@asymmetrik/ngx-leaflet`
`@asymmetrik/ngx-leaflet-draw`
`bootstrap`
`ngx-bootstrap`

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';

import { AppComponent } from './app.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalContentComponent } from './modal';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ModalContentComponent
  ],
  imports: [
    BrowserModule,
    LeafletModule.forRoot(),
    LeafletDrawModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule
  ],
  entryComponents: [ModalContentComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, TemplateRef } from '@angular/core';
import { Map, point, marker, icon, Marker } from 'leaflet';
import * as L from 'leaflet';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalContentComponent } from 'src/app/modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  bsModalRef: BsModalRef;
  constructor(private modalService: BsModalService, private sanitizer: DomSanitizer) { }

  markers: Marker[] = [];
  map: Map;
  show: false;
  htmlData: any;

  ngOnInit() {
    this.markers = new Array();
  }
  public openModalWithComponent() {
    this.bsModalRef = this.modalService.show(ModalContentComponent);
  }

  title = 'wolfchatter';
  options = {
    layers: [
      L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'Open Street Map' })
    ],
    zoom: 5,
    center: L.latLng({ lat: 46.879966, lng: -121.726909 })
  };

  drawOptions = {
    position: 'topright',
    draw: {
      marker: {
        icon: L.icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/map_marker.svg',
        }),
        draggable: true

      }
    }
  };

  onMapReady(map: Map) {
    this.map = map;
    let popup = L.popup({
      closeButton: false,
      autoClose: false
    })
      .setLatLng([46.879966, -121.726909])
      .setContent('<p>Click on the map to start a chat</p>')
      .openOn(map);
  }
  onDrawCreated(e: any) {

  }
  onDrawStart(e: any) {

  }
  onLeafletClick(e: any) {
    const newMarker = marker(
      [e.latlng.lat, e.latlng.lng],
      {
        icon: icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: 'assets/map_marker.svg',
        }),
        draggable: true
      }
    );

    this.markers.push(newMarker);
    this.openModalWithComponent();
  }
  removeClickedMarker() {
    this.markers.pop();
  }
}

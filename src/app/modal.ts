import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'modal-content',
    template: `
    <div class="modal-header">
      <p class="header-text">Chatroom {{chatroomNr}}</p>
    </div>
    <div class="modal-body">
      <div class="row" *ngFor="let message of messages">
        <small class="col-md-2">{{message.username}}</small>
        <small class="col-md-5">{{message.text}}</small>
        <small class="col-md-5">{{message.date}}</small>
      </div>
    </div>
    <div class="modal-footer">
    <div class="row">
        <div class="col-md-6">
        <input type="text" placeholder="Enter username" [(ngModel)]="username" class="form-control" id="username">
        </div>
        <div class="col-md-6">
        <input type="text" placeholder="Enter your message" [(ngModel)]="message" class="form-control" id="text">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <button type="button" (click)="sendMessage()" class="btn btn-wolfchatter">
                <span class="btn-text">Send</span>
            </button>
        </div>
    </div>
    </div>
  `
})

export class ModalContentComponent implements OnInit {

    public title: string;
    public list: any[] = [];
    username: string;
    message: string;
    messages: any;
    chatroomNr: number;

    ngOnInit() {
        this.messages = new Array();
        this.getMessages();
    }

    getMessages() {
        this.messages = JSON.parse(localStorage.getItem('messages'));
        if (this.messages === null) {
            this.messages = new Array();
        }
        console.log(this.messages);
    }
    sendMessage() {
        this.messages.push({
            username: this.username,
            text: this.message,
            date: new Date().toLocaleString()
        });
        localStorage.setItem('messages', JSON.stringify(this.messages));
    }
}